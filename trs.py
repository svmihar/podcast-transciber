import wavTranscriber
from fire import Fire
import time
from yt import download
from tqdm import tqdm
import numpy as np
from pathlib import Path

# pathc containing pretrained mdoels
model_path = Path("./models")
pb, scorer = wavTranscriber.resolve_models(str(model_path))

model = wavTranscriber.load_model(pb, scorer)


def transcribe(wav_file_path):
    aggresiveness = 2
    segments, sample_rate, _ = wavTranscriber.vad_segment_generator(
        wav_file_path, aggresiveness
    )
    f = open(wav_file_path.rstrip(".wav") + ".txt", "w")

    for i, segment in enumerate(tqdm([s for s in segments])):
        audio = np.frombuffer(segment[0], dtype=np.int16)
        output = wavTranscriber.stt(model[0], audio, sample_rate)
        timestamp = time.strftime("%M:%S", time.gmtime(segment[1][1]))
        f.write(f"{timestamp} {output[0]}\n")
    f.close()


def main(yt_link):
    transcribe(download(yt_link))


if __name__ == "__main__":
    Fire()
