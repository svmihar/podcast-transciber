# kurang
- [x] converter dari youtubelink ke 16000hz wav
    - need to find a way to compress the wav file.
    - dari m4a ke wav -> 9.6MB

- [ ] frontend buat serving model
- [ ] editor buat ngedit timestamp / isi timestampnya
- [ ] bikin pypackage pake poetry

 # models
 ```bash
# Install DeepSpeech
pip3 install deepspeech

# Download pre-trained English model files
curl -LO https://github.com/mozilla/DeepSpeech/releases/download/v0.8.1/deepspeech-0.8.1-models.pbmm
curl -LO https://github.com/mozilla/DeepSpeech/releases/download/v0.8.1/deepspeech-0.8.1-models.scorer
 ```