import pandas as pd
import time
import re


def parse_time(time_string):
    return time_string.split(',')[0]


def parse_srt(srt_string):
    srt_list = []

    for line in srt_string.decode('utf-8').split("\n\n"):
        if line == "":
            continue

        pos = re.search(r"\d+:\d+:\d+,\d+ --> \d+:\d+:\d+,\d+", line).end() + 1
        content = line[pos:]
        start_time_string = re.findall(r"(\d+:\d+:\d+,\d+) --> \d+:\d+:\d+,\d+", line)[
            0
        ]
        start_time = parse_time(start_time_string)

        srt_list.append(
            {"text": content, "start": start_time,'time': start_time_string}
        )

    return pd.DataFrame(srt_list)


if __name__ == "__main__":

    # FILE = [a.replace("\n", "") for a in open("test.srt").read().splitlines() if a]
    srt = open("./test.srt").read()
    parsed = parse_srt(srt)
    breakpoint()
