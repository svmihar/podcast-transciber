from vector import dbscan
from process import parse_srt
from pathlib import Path
from flask import Flask, request, url_for
from werkzeug.utils import secure_filename


app=Flask(__name__)
app.config['UPLOAD_FOLDER'] = './temp'

def allowed_filename(filename):
    suffix = Path(filename).suffix
    return suffix == 'srt'


def dataframe(srt_file):
    return dbscan(parse_srt(srt_file), {'algorithm': 'auto', 'eps': 0.01, 'leaf_size': 30, 'metric': 'euclidean',
                      'metric_params': None, 'min_samples': 9, 'n_jobs': None, 'p': None})

@app.route('/api/srt', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files
        print(file['item'])
        df, *_ = dataframe(file['item'].read())
        df.to_csv('from_flask.csv', index=False)
    return {'topics': df.to_dict('records')}
if __name__ == "__main__":
    app.run(debug=True)