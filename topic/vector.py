from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import MinMaxScaler
from tqdm import tqdm
from sklearn.metrics import silhouette_score
from sklearn.decomposition import PCA
import pandas as pd
from sklearn.cluster import DBSCAN
from process import parse_srt
from db import init


def vectorize(list_of_string):
    vectorizer = TfidfVectorizer()
    v = vectorizer.fit_transform(list_of_string).toarray()
    v_ = PCA(n_components=2).fit_transform(v)
    return v_


def dbscan(df, params):
    vectors = vectorize(df["text"])
    dbscan = DBSCAN().set_params(**params)
    dbscan.fit(vectors)
    print(len(set(dbscan.labels_)), "topics found")
    df["topic"] = dbscan.labels_
    score, noise = silhouette_score(
        vectors, dbscan.labels_), list(dbscan.labels_).count(-1)
    params = dbscan.get_params()
    return df, score, noise, params


def hyperopt(df, list_params):
    result = []
    for params in tqdm(list_params):
        _, score, noise,  p = dbscan(df, params)
        result.append(
            {'noise': noise,
             'score': score,
             'params': p}
        )
    return result


def permutation(a1, a2):
    for x in a1:
        for y in a2:
            yield (x, y)


def normalize(df):
    rs = MinMaxScaler()
    X = rs.fit_transform(df['score'].values.reshape(-1, 1))
    X = [a[0] for a in X]
    X_ = rs.fit_transform(df['noise'].values.reshape(-1, 1))
    X_ = [1-a[0] for a in X_]
    df['joined'] = [a+b for a, b in zip(X, X_)]
    df = df.sort_values(by='joined', ascending=False)
    df.to_csV('test_optimized.csv', index=False)
    return df


def generate_params():
    eps = [x*.01 for x in range(1, 10)]
    min_samples = [x for x in range(2, 11)]
    return [{'eps': a, 'min_samples': b} for a, b in permutation(eps, min_samples)]
    # return [{'eps': a, 'min_samples': b} for a, b in zip(eps, min_samples)]


if __name__ == "__main__":
    df_: pd.DataFrame = parse_srt(open("./test.srt").read())
    df_.pipe(dbscan, {'algorithm': 'auto', 'eps': 0.01, 'leaf_size': 30, 'metric': 'euclidean',
                      'metric_params': None, 'min_samples': 9, 'n_jobs': None, 'p': None})
    # df_.to_csv("test.csv", index=False)
    db= init('joe_rogan')
    db.insert_many(df_.to_dict('records'))

    ''' ---- optiziming epsilon, and min_samples '''
    # test_params = generate_params()
    # df_result = pd.DataFrame(hyperopt(df_, test_params)).pipe(normalize)
    # breakpoint()
