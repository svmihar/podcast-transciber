from pathlib import Path
from pytube import YouTube
import sys
from pydub import AudioSegment


def convert_wav(fpath):
    filename, ext = fpath.split("/")[-1].split(".")
    webm = AudioSegment.from_file(fpath, ext)
    print("converting", filename)
    f = f"src/{filename}.wav"
    file_handle = webm.export(
        f, format="wav", parameters="-ar 16000 -ac 1 -acodec pcm_s16le".split()
    )
    return f


def download(yt_link):
    y = YouTube(yt_link)
    audio = y.streams.filter(only_audio=True)[2]
    wav_file = convert_wav(audio.download("./src/"))
    delete_m4a("./src")
    return wav_file


def delete_m4a(src_path):
    m4a_files = sorted(Path(src_path).rglob("*.webm"))
    for m4a in m4a_files:
        print(m4a)
        m4a.unlink()


if __name__ == "__main__":
    args = sys.argv[1:]
    if len(args) < 1:
        raise ValueError("kurang argumen jng.")
    link = args[0]
    download(link)
